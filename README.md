# What is this repository for? #

PCM-based: A C/C++ library for implementing XOR-based Erasure Codes.  
[What is erasure code?](https://en.wikipedia.org/wiki/Erasure_code)

## Features ##

* High decoding speed (especially for erasure codes tolerating 3 or more failures), which thanks to the efficient decoding algorithm based on the _parity-check matrix_.
* Contain several popular erasure codes (such as RDP, STAR, TIP-code, etc). 

## Installation ##

The installation requires *cmake* to be installed in advance.

```
$ mkdir build
$ cd build
$ cmake ..
$ make
```

To see whether the installation succeeds, just try:

```
$ ./validate tip 12
```

Then the output should be:

```
$ 12  2.6667 3.0528 TIP-code 3 10
```

## External Material ##

Yongzhe Zhang, Chentao Wu, Jie Li, Minyi Guo, _"PCM: A Parity-check Matrix based Approach to Implement Erasure Codes with High Update Complexity"_, The 34th International Symposium on Storage and Reliable Systems (SRDS'15).

## Contact ##

Author: Yongzhe Zhang  
Email: zyz915@gmail.com