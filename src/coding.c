/*
 * coding.c
 *
 *  Created on: Nov 9, 2014
 *      Author: zyz915
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "coding.h"

#define ID(r, c) ((c) * w + (r))

static void codeword_shortening(coding_t *coding, int shortened, int start)
{
	// assertion: removed columns do not contain parity elements
	int w = coding->w, n = coding->n, cols = coding->cols, i, j;
	for (i = 0; i < shortened * w; i++)
		if (coding->col_info[start * w + i] > -1) {
			fprintf(stderr, "shortened columns contain parity elements.\n");
			exit(0);
		}
	// modify parity-check matrix
	int *pcm = coding->pcm, bits = shortened * w;
	for (i = 0; i < coding->rows; i++) {
		int *pcmr = coding->pcm + i * cols;
		for (j = 0; j < cols; j++)
			if (j < start * w || j >= (start + shortened) * w)
				*pcm++ = pcmr[j];
	}
	// modify metadata
	for (i = start * w; i < (n - shortened) * w; i++)
		coding->col_info[i] = coding->col_info[i + bits];
	for (i = 0; i < coding->rows; i++)
		if (coding->row_info[i] >= start * w)
			coding->row_info[i] -= bits;
	coding->cols -= bits;
	coding->data_len -= bits;
	coding->k -= shortened;
	coding->n -= shortened;
	int *ptr = coding->data_place;
	for (i = 0; i < w; i++)
		for (j = 0; j < coding->n; j++)
			if (coding->col_info[ID(i, j)] == -1)
				*ptr++ = ID(i, j);
}

static coding_t* make_coding_rdp(int n)
{
	int shortened = 0;
	while (!pcm_is_prime(n - 1)) n++, shortened++;
	coding_t *coding = (coding_t*) malloc(sizeof(coding_t));
	coding->n = n;
	int p = coding->p = n - 1;
	int m = coding->m = 2;
	int k = coding->k = n - m;
	int w = coding->w = p - 1;
	int rows = coding->rows = m * w;
	int cols = coding->cols = n * w;
	int *row_info = coding->row_info = (int*) malloc(sizeof(int) * rows);
	int *col_info = coding->col_info = (int*) malloc(sizeof(int) * cols);
	int *pcm = coding->pcm = (int*) malloc(sizeof(int) * rows * cols);
	int *data = coding->data_place = (int*) malloc(sizeof(int) * k * w);
	int data_len = coding->data_len = k * w;
	memset(pcm, 0, sizeof(int) * rows * cols);
	int i, r, c, nr = 0;
	// data location
	for (i = 0; i < k * w; i++) {
		r = i / k, c = i % k;
		data[i] = ID(r, c);
		col_info[ID(r, c)] = -1;
	}
	// horizontal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			pcmr[ID(r, c)] = 1;
		row_info[nr] = ID(r, p - 1);
		col_info[ID(r, p - 1)] = nr;
		nr++;
	}
	// diagonal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			if ((r - c + p + 1) % p)
				pcmr[ID((r - c + p) % p, c)] = 1;
		pcmr[ID(r, p)] = 1;
		row_info[nr] = ID(r, p);
		col_info[ID(r, p)] = nr;
		nr++;
	}
	codeword_shortening(coding, shortened, 0);
	return coding;
}

static coding_t* make_coding_evenodd(int n)
{
	int shortened = 0;
	while (!pcm_is_prime(n - 2)) n++, shortened++;
	coding_t *coding = (coding_t*) malloc(sizeof(coding_t));
	coding->n = n;
	int p = coding->p = n - 2;
	int m = coding->m = 2;
	int k = coding->k = n - m;
	int w = coding->w = p - 1;
	int rows = coding->rows = m * w + 1;
	int cols = coding->cols = n * w + 1;
	int *row_info = coding->row_info = (int*) malloc(sizeof(int) * rows);
	int *col_info = coding->col_info = (int*) malloc(sizeof(int) * cols);
	int *pcm = coding->pcm = (int*) malloc(sizeof(int) * rows * cols);
	int *data = coding->data_place = (int*) malloc(sizeof(int) * k * w);
	int data_len = coding->data_len = k * w;
	memset(pcm, 0, sizeof(int) * rows * cols);
	int i, r, c, nr = 0, S = n * w;
	// data location
	for (i = 0; i < k * w; i++) {
		r = i / k, c = i % k;
		data[i] = ID(r, c);
		col_info[ID(r, c)] = -1;
	}
	// horizontal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p + 1; c++)
			pcmr[ID(r, c)] = 1;
		row_info[nr] = ID(r, p);
		col_info[ID(r, p)] = nr;
		nr++;
	}
	// diagonal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			if ((r - c + p + 1) % p)
				pcmr[ID((r - c + p) % p, c)] = 1;
		pcmr[ID(r, p + 1)] = 1;
		pcmr[S] = 1; // special S
		row_info[nr] = ID(r, p + 1);
		col_info[ID(r, p + 1)] = nr;
		nr++;
	}
	// special S
	int *pcmr = pcm + nr * cols;
	for (c = 1; c < p; c++) {
		r = w - c;
		pcmr[ID(r, c)] = 1;
	}
	pcmr[S] = 1;
	row_info[nr] = S;
	col_info[S] = nr;
	codeword_shortening(coding, shortened, 1);
	return coding;
}

static coding_t* make_coding_star(int n)
{
	int shortened = 0;
	while (!pcm_is_prime(n - 3)) n++, shortened++;
	coding_t *coding = (coding_t*) malloc(sizeof(coding_t));
	coding->n = n;
	int p = coding->p = n - 3;
	int m = coding->m = 3;
	int k = coding->k = n - m;
	int w = coding->w = p - 1;
	int rows = coding->rows = m * w + 2;
	int cols = coding->cols = n * w + 2;
	int *row_info = coding->row_info = (int*) malloc(sizeof(int) * rows);
	int *col_info = coding->col_info = (int*) malloc(sizeof(int) * cols);
	int *pcm = coding->pcm = (int*) malloc(sizeof(int) * rows * cols);
	int *data = coding->data_place = (int*) malloc(sizeof(int) * k * w);
	int data_len = coding->data_len = k * w;
	memset(pcm, 0, sizeof(int) * rows * cols);
	int i, r, c, nr = 0, S1 = n * w, S2 = S1 + 1;
	// data location
	for (i = 0; i < k * w; i++) {
		r = i / k, c = i % k;
		data[i] = ID(r, c);
		col_info[ID(r, c)] = -1;
	}
	// horizontal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p + 1; c++)
			pcmr[ID(r, c)] = 1;
		row_info[nr] = ID(r, p);
		col_info[ID(r, p)] = nr;
		nr++;
	}
	// diagonal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			if ((r - c + p + 1) % p)
				pcmr[ID((r - c + p) % p, c)] = 1;
		pcmr[ID(r, p + 1)] = 1;
		pcmr[S1] = 1; // special S1
		row_info[nr] = ID(r, p + 1);
		col_info[ID(r, p + 1)] = nr;
		nr++;
	}
	// anti-diagonal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			if ((r + c + 1) % p)
				pcmr[ID((r + c) % p, c)] = 1;
		pcmr[ID(r, p + 2)] = 1;
		pcmr[S2] = 1; // special S2
		row_info[nr] = ID(r, p + 2);
		col_info[ID(r, p + 2)] = nr;
		nr++;
	}
	// special S1
	int *pcmr = pcm + nr * cols;
	for (c = 1; c < p; c++) {
		r = w - c;
		pcmr[ID(r, c)] = 1;
	}
	pcmr[S1] = 1;
	row_info[nr] = S1;
	col_info[S1] = nr;
	nr++;
	// special S2
	pcmr = pcm + nr * cols;
	for (c = 1; c < p; c++) {
		r = c - 1;
		pcmr[ID(r, c)] = 1;
	}
	pcmr[S2] = 1;
	row_info[nr] = S2;
	col_info[S2] = nr;
	codeword_shortening(coding, shortened, 1);
	return coding;
}

static coding_t* make_coding_triple_star(int n)
{
	int shortened = 0;
	while (!pcm_is_prime(n - 2)) n++, shortened++;
	coding_t *coding = (coding_t*) malloc(sizeof(coding_t));
	coding->n = n;
	int p = coding->p = n - 2;
	int m = coding->m = 3;
	int k = coding->k = n - m;
	int w = coding->w = p - 1;
	int rows = coding->rows = m * w;
	int cols = coding->cols = n * w;
	int *row_info = coding->row_info = (int*) malloc(sizeof(int) * rows);
	int *col_info = coding->col_info = (int*) malloc(sizeof(int) * cols);
	int *pcm = coding->pcm = (int*) malloc(sizeof(int) * rows * cols);
	int *data = coding->data_place = (int*) malloc(sizeof(int) * k * w);
	int data_len = coding->data_len = k * w;
	memset(pcm, 0, sizeof(int) * rows * cols);
	int i, r, c, nr = 0;
	// data location
	for (i = 0; i < k * w; i++) {
		r = i / k, c = i % k;
		data[i] = ID(r, c);
		col_info[ID(r, c)] = -1;
	}
	// horizontal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			pcmr[ID(r, c)] = 1;
		row_info[nr] = ID(r, p - 1);
		col_info[ID(r, p - 1)] = nr;
		nr++;
	}
	// anti-diagonal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			if ((r + c + 1) % p)
				pcmr[ID((r + c) % p, c)] = 1;
		pcmr[ID(r, p)] = 1;
		row_info[nr] = ID(r, p);
		col_info[ID(r, p)] = nr;
		nr++;
	}
	// diagonal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			if ((r - c + p + 1) % p)
				pcmr[ID((r - c + p) % p, c)] = 1;
		pcmr[ID(r, p + 1)] = 1;
		row_info[nr] = ID(r, p + 1);
		col_info[ID(r, p + 1)] = nr;
		nr++;
	}
	codeword_shortening(coding, shortened, 0);
	return coding;
}

static coding_t* make_coding_liberation(int n)
{
	int shortened = 0;
	while (!pcm_is_prime(n - 2)) n++, shortened++;
	coding_t *coding = (coding_t*) malloc(sizeof(coding_t));
	coding->n = n;
	int p = coding->p = n - 2;
	int m = coding->m = 2;
	int k = coding->k = n - m;
	int w = coding->w = p;
	int rows = coding->rows = m * w;
	int cols = coding->cols = n * w;
	int *row_info = coding->row_info = (int*) malloc(sizeof(int) * rows);
	int *col_info = coding->col_info = (int*) malloc(sizeof(int) * cols);
	int *pcm = coding->pcm = (int*) malloc(sizeof(int) * rows * cols);
	int *data = coding->data_place = (int*) malloc(sizeof(int) * k * w);
	int data_len = coding->data_len = k * w;
	memset(pcm, 0, sizeof(int) * rows * cols);
	int i, r, c, nr = 0;
	// data location
	for (i = 0; i < k * w; i++) {
		r = i / k, c = i % k;
		data[i] = ID(r, c);
		col_info[ID(r, c)] = -1;
	}
	// horizontal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			pcmr[ID(r, c)] = 1;
		pcmr[ID(r, p)] = 1;
		row_info[nr] = ID(r, p);
		col_info[ID(r, p)] = nr;
		nr++;
	}
	// vertical parity
	for (c = 1; c < p; c++) {
		int y = c * (p - 1) / 2 % p;
		int *pcmr = pcm + (w + y) * cols;
		pcmr[ID((y + c - 1) % p, c)] = 1;
	}
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			pcmr[ID((r + c) % p, c)] = 1;
		pcmr[ID(r, p + 1)] = 1;
		row_info[nr] = ID(r, p + 1);
		col_info[ID(r, p + 1)] = nr;
		nr++;
	}
	codeword_shortening(coding, shortened, 1);
	return coding;
}

static coding_t* make_coding_hover(int n)
{
	if (!pcm_is_prime(n - 1)) return NULL;
	coding_t* coding = (coding_t*) malloc(sizeof(coding_t));
	coding->n = n;
	int p = coding->p = n - 1;
	int m = coding->m = 3;
	int k = coding->k = n - m;
	int w = coding->w = p - 1;
	int rows = coding->rows = m * w;
	int cols = coding->cols = n * w;
	int *row_info = coding->row_info = (int*) malloc(sizeof(int) * rows);
	int *col_info = coding->col_info = (int*) malloc(sizeof(int) * cols);
	int *pcm = coding->pcm = (int*) malloc(sizeof(int) * rows * cols);
	int *data = coding->data_place = (int*) malloc(sizeof(int) * k * w);
	int data_len = coding->data_len = (w - 2) * p;
	memset(pcm, 0, sizeof(int) * rows * cols);
	int i, r, c, nr = 0;
	// data location
	for (i = 0; i < (w - 2) * p; i++) {
		r = i / p, c = i % p;
		data[i] = ID(r, c);
		col_info[ID(r, c)] = -1;
	}
	col_info[n * w - 1] = -2;
	col_info[n * w - 2] = -2;
	// up diagonal
	for (c = 0; c < p; c++) {
		int *pcmr = pcm + nr * cols;
		for (r = 0; r < w - 2; r++)
			pcmr[ID(r, (c + p - 2 - r) % p)] = 1;
		pcmr[ID(p - 3, c)] = 1;
		row_info[nr] = ID(p - 3, c);
		col_info[ID(p - 3, c)] = nr;
		nr++;
	}
	// down diagonal
	for (c = 0; c < p; c++) {
		int *pcmr = pcm + nr * cols;
		for (r = 0; r < w - 2; r++)
			pcmr[ID(r, (c + 2 + r) % p)] = 1;
		pcmr[ID(p - 2, c)] = 1;
		row_info[nr] = ID(p - 2, c);
		col_info[ID(p - 2, c)] = nr;
		nr++;
	}
	// horizontal
	for (r = 0; r < w - 2; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			pcmr[ID(r, c)] = 1;
		pcmr[ID(r, p)] = 1;
		row_info[nr] = ID(r, p);
		col_info[ID(r, p)] = nr;
		nr++;
	}
	return coding;
}

static coding_t* make_coding_tip_code(int n)
{
	if (!pcm_is_prime(n - 1)) return NULL;
	coding_t* coding = (coding_t*) malloc(sizeof(coding_t));
	coding->n = n;
	int p = coding->p = n - 1;
	int m = coding->m = 3;
	int k = coding->k = n - m;
	int w = coding->w = p - 1;
	int rows = coding->rows = m * w;
	int cols = coding->cols = n * w;
	int *row_info = coding->row_info = (int*) malloc(sizeof(int) * rows);
	int *col_info = coding->col_info = (int*) malloc(sizeof(int) * cols);
	int *pcm = coding->pcm = (int*) malloc(sizeof(int) * rows * cols);
	int *data = coding->data_place = (int*) malloc(sizeof(int) * k * w);
	int data_len = coding->data_len = k * w;
	memset(pcm, 0, sizeof(int) * rows * cols);
	int i, r, c, nr = 0, nr_d = 0;
	// data location
	for (r = 0; r < w; r++)
		for (c = 0; c < p; c++)
			if (r + 1 != c && r + c != p - 1) {
				data[nr_d++] = ID(r, c);
				col_info[ID(r, c)] = -1;
			}
	// diagonal parity
	for (i = 0; i < p - 1; i++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++) {
			r = (p + i - c) % p;
			if (r != p - 1 && r + 1 != c && r + c != p - 1)
				pcmr[ID(r, c)] = 1;
		}
		pcmr[ID(i, i + 1)] = 1;
		row_info[nr] = ID(i, i + 1);
		col_info[ID(i, i + 1)] = nr;
		nr++;
	}
	// anti-diagonal parity
	for (i = 0; i < p - 1; i++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++) {
			int r = (c + i) % p;
			if (r != p - 1 && r + 1 != c && r + c != p - 1)
				pcmr[ID(r, c)] = 1;
		}
		pcmr[ID(i, p - 1 - i)] = 1;
		row_info[nr] = ID(i, p - 1 - i);
		col_info[ID(i, p - 1 - i)] = nr;
		nr++;
	}
	// horizontal parity
	for (r = 0; r < p - 1; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p + 1; c++)
			if (r + 1 != c && r + c != p - 1)
				pcmr[ID(r, c)] = 1;
		row_info[nr] = ID(r, p);
		col_info[ID(r, p)] = nr;
		nr++;
	}
	return coding;
}

static coding_t* make_coding_ehcode(int n)
{
	if (!pcm_is_prime(n - 2)) return NULL;
	coding_t* coding = (coding_t*) malloc(sizeof(coding_t));
	coding->n = n;
	int p = coding->p = n - 2;
	int m = coding->m = 3;
	int k = coding->k = n - m;
	int w = coding->w = p - 1;
	int rows = coding->rows = m * w;
	int cols = coding->cols = n * w;
	int *row_info = coding->row_info = (int*) malloc(sizeof(int) * rows);
	int *col_info = coding->col_info = (int*) malloc(sizeof(int) * cols);
	int *pcm = coding->pcm = (int*) malloc(sizeof(int) * rows * cols);
	int *data = coding->data_place = (int*) malloc(sizeof(int) * k * w);
	int data_len = coding->data_len = k * w;
	memset(pcm, 0, sizeof(int) * rows * cols);
	int i, r, c, nr = 0, nr_d = 0;
	// data location
	for (r = 0; r < w; r++)
		for (c = 0; c < p; c++)
			if (r + 1 != c) {
				data[nr_d++] = ID(r, c);
				col_info[ID(r, c)] = -1;
			}
	// horizontal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			if (r + 1 != c)
				pcmr[ID(r, c)] = 1;
		pcmr[ID(r, p)] = 1;
		row_info[nr] = ID(r, p);
		col_info[ID(r, p)] = nr;
		nr++;
	}
	// anti-diagonal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			if ((c + p - r - 1) % p)
				pcmr[ID((c + p - r - 2) % p, c)] = 1;
		pcmr[ID(r, r + 1)] = 1;
		row_info[nr] = ID(r, r + 1);
		col_info[ID(r, r + 1)] = nr;
		nr++;
	}
	// diagonal parity
	for (i = 0; i < w; i++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++) {
			int r = (i + p - c) % p;
			if (r + 1 != c) {
				if (r + 1 == p)
					r = c - 1;
				pcmr[ID(r, c)] = 1;
			}
		}
		pcmr[ID(i, p + 1)] = 1;
		row_info[nr] = ID(i, p + 1);
		col_info[ID(i, p + 1)] = nr;
		nr++;
	}
	return coding;
}

static coding_t* make_coding_hdd1(int n)
{
	if (!pcm_is_prime(n - 1)) return NULL;
	coding_t* coding = (coding_t*) malloc(sizeof(coding_t));
	coding->n = n;
	int p = coding->p = n - 1;
	int m = coding->m = 3;
	int k = coding->k = n - m;
	int w = coding->w = p - 1;
	int rows = coding->rows = m * w;
	int cols = coding->cols = n * w;
	int *row_info = coding->row_info = (int*) malloc(sizeof(int) * rows);
	int *col_info = coding->col_info = (int*) malloc(sizeof(int) * cols);
	int *pcm = coding->pcm = (int*) malloc(sizeof(int) * rows * cols);
	int *data = coding->data_place = (int*) malloc(sizeof(int) * k * w);
	int data_len = coding->data_len = k * w;
	memset(pcm, 0, sizeof(int) * rows * cols);
	int i, r, c, nr = 0, nr_d = 0;
	// data location
	for (r = 0; r < p - 2; r++)
		for (c = 0; c < p; c++)
			if (r + c != p - 1) {
				data[nr_d++] = ID(r, c);
				col_info[ID(r, c)] = -1;
			}
	// horizontal parity
	for (r = 0; r < p - 2; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			if (r + c != p - 1)
				pcmr[ID(r, c)] = 1;
		pcmr[ID(r, p - 1 - r)] = 1;
		row_info[nr] = ID(r, p - 1 - r);
		col_info[ID(r, p - 1 - r)] = nr;
		nr++;
	}
	// diagonal parity
	for (i = 0; i < p; i++) {
		int *pcmr = pcm + nr * cols, sumrc = i + p - 2;
		for (r = 0; r < p - 2; r++)
			pcmr[ID(r, (sumrc + p - r) % p)] = 1;
		pcmr[ID(p - 2, i)] = 1;
		row_info[nr] = ID(p - 2, i);
		col_info[ID(p - 2, i)] = nr;
		nr++;
	}
	// diagonal parity
	for (r = 0; r < w; r++) {
		int *pcmr = pcm + nr * cols;
		for (c = 0; c < p; c++)
			if ((r + c + 1) % p)
				pcmr[ID((r + c) % p, c)] = 1;
		pcmr[ID(r, p)] = 1;
		row_info[nr] = ID(r, p);
		col_info[ID(r, p)] = nr;
		nr++;
	}
	return coding;
}

coding_t* make_coding(int tech, int n)
{
	coding_t *ret = NULL;
	switch (tech) {
	case RDP:
		ret = make_coding_rdp(n); break;
	case EVENODD:
		ret = make_coding_evenodd(n); break;
	case STAR:
		ret = make_coding_star(n); break;
	case Triple_Star:
		ret = make_coding_triple_star(n); break;
	case Liberation:
		ret = make_coding_liberation(n); break;
	case HoVer:
		ret = make_coding_hover(n); break;
	case EH_code:
		ret = make_coding_ehcode(n); break;
	case HDD1:
		ret = make_coding_hdd1(n); break;
	case X_code:
	case H_code:
	case TIP_code:
		ret = make_coding_tip_code(n); break;
	default:
		fprintf(stderr, "not supported yet\n");
		exit(0);
	}
	if (ret == NULL) {
		fprintf(stderr, "invalid disk array size: %d\n", n);
		exit(0);
	}
	return ret;
}

const char *coding_name(int tech)
{
	switch (tech) {
	case EVENODD:
		return "EVENODD";
	case RDP:
		return "RDP";
	case X_code:
		return "X-code";
	case H_code:
		return "H-code";
	case Liberation:
		return "Liberation";
	case STAR:
		return "STAR";
	case Triple_Star:
		return "Triple-Star";
	case HoVer:
		return "HoVer";
	case TIP_code:
		return "TIP-code";
	case EH_code:
		return "EH-code";
	case HDD1:
		return "HDD1";
	default:
		return "";
	}
}

void list_coding_techniques(FILE *f)
{

}
