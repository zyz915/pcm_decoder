/*
 * pcm_based.c
 *
 *  Created on: Oct 15, 2014
 *      Author: zyz915
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "pcm_based.h"

#define ID(r, c) ((c) * w + (r))

int pcm_is_prime(int n)
{
	int i;
	for (i = 2; i * i <= n; i++)
		if (n % i == 0)
			return 0;
	return 1;
}

void pcm_print_bitmatrix(int *bitm, int rows, int cols)
{
	int i, j;
	for (i = 0; i < rows; i++) {
		for (j = 0; j < cols; j++)
			printf("%d ", *bitm++);
		printf("\n");
	}
	printf("\n");
}

static void inline add_u32(uint32_t *dst, uint32_t *src, int n)
{
	uint32_t *stop = dst + n;
	while (dst < stop)
		*dst++ ^= *src++;
}

static void inline swap_u32(uint32_t *dst, uint32_t *src, int n)
{
	uint32_t *stop = dst + n;
	while (dst < stop) {
		*src ^= *dst ^= *src;
		*dst++ ^= *src++;
	}
}

static void invert_bitmatrix(int *bitm, int n)
{
	int w = (n >> 5) + (!!(n & 31)), w2 = w * 2, i;
	uint32_t *tmp = (uint32_t*) malloc(sizeof(uint32_t) * n * w2);
	memset(tmp, 0, sizeof(uint32_t) * n * w2);
	for (i = 0; i < n; i++) {
		int *t = bitm + i * n, j;
		uint32_t *t2 = tmp + i * w2;
		for (j = 0; j < n; j++)
			t2[j >> 5] ^= ((uint32_t)t[j]) << (j & 31);
		t2[w + (i >> 5)] ^= 1u << (i & 31);
	}
	for (i = 0; i < n; i++) {
		int ch = i, o1 = i >> 5, o2 = i & 31;
		while (ch < n && !(1 & (tmp[ch * w2 + o1] >> o2))) ++ch;
		if (ch != i)
			swap_u32(tmp + ch * w2, tmp + i * w2, w2);
		for (++ch; ch < n; ++ch)
			if (1 & (tmp[ch * w2 + o1] >> o2))
				add_u32(tmp + ch * w2, tmp + i * w2, w2);
	}
	for (i = n - 1; i >= 0; i--) {
		int ch, o1 = i >> 5, o2 = i & 31;
		for (ch = i - 1; ch > -1; --ch)
			if (1 & (tmp[ch * w2 + o1] >> o2))
				add_u32(tmp + ch * w2, tmp + i * w2, w2);
	}
	for (i = 0; i < n; i++) {
		int *t = bitm + i * n, j;
		uint32_t *t2 = tmp + i * w2 + w;
		for (j = 0; j < n; j++)
			t[j] = 1 & (t2[j >> 5] >> (j & 31));
	}
	free(tmp);
}

static void pseudo_inverse(int *bitm, int rows, int cols)
{
	int i, j, k, p = 0;
	int *ret = (int*) malloc(sizeof(int) * rows * cols);
	int *picked = (int*) malloc(sizeof(int) * rows);
	for (i = 0; i < rows; i++) {
		memcpy(ret + p * cols, bitm + i * cols, sizeof(int) * cols);
		int *r = ret + p * cols;
		for (j = 0; j < p; j++) {
			int ch = 0, *b = ret + j * cols;
			while (b[ch] == 0) ch++;
			if (r[ch])
				for (k = 0; k < cols; k++)
					r[k] ^= b[k];
		}
		int iszero = 1;
		for (j = 0; j < cols; j++)
			if (r[j]) iszero = 0;
		if (!iszero) {
			if (i > p)
				memcpy(bitm + p * cols, bitm + i * cols, sizeof(int) * cols);
			picked[p++] = i;
		}
	}
	invert_bitmatrix(bitm, cols);
	memcpy(ret, bitm, sizeof(int) * cols * cols);
	memset(bitm, 0, sizeof(int) * rows * cols);
	for (i = 0; i < cols; i++) {
		int *t = ret + i * cols;
		int *b = bitm + i * rows;
		for (j = 0; j < cols; j++)
			b[picked[j]] = t[j];
	}
	free(picked);
	free(ret);
}

static void pcm_rank_bitmatrix(int *bitm, int rows, int cols)
{
	int i, j, k, p = 0;
	int *tmp = (int*) malloc(sizeof(int) * rows * cols);
	for (i = 0; i < rows; i++) {
		memcpy(tmp + p * cols, bitm + i * cols, sizeof(int) * cols);
		int *r = tmp + p * cols;
		for (j = 0; j < p; j++) {
			int ch = 0, *b = tmp + j * cols;
			while (b[ch] == 0) ch++;
			if (r[ch])
				for (k = 0; k < cols; k++)
					r[k] ^= b[k];
		}
		int iszero = 1;
		for (j = 0; j < cols; j++)
			if (r[j]) iszero = 0;
		if (!iszero) {
			if (i > p)
				memcpy(bitm + p * cols, bitm + i * cols, sizeof(int) * cols);
			p++;
		}
	}
	free(tmp);
}

static void do_scheduled_operations(sched_t *sched, char **buf, int packetsize)
{
	while (sched->op != -1) {
		int op = sched->op;
		char *src = buf[sched->src];
		char *dst = buf[sched->dst];
		if (op == 1) {
			char *end = dst + packetsize;
			while (dst < end) *dst++ ^= *src++;
		} else
			memcpy(dst, src, packetsize);
		sched++;
	}
}

static sched_t* optimize_decode_schedule(coding_t *coding, sched_t *sched, int *erased)
{
	int n = coding->n, w = coding->w, i;
	int rows = coding->rows, cols = coding->cols;
	char *tmp = (char*) malloc(rows + cols);
	memset(tmp, 1, rows + cols);
	memset(tmp, 0, n * w);
	sched_t *s;
	// check zero values
	for (s = sched; s->op != -1; s++) {
		if (s->op == 0) { // copy
			if (tmp[s->src])
				s->op = -2; // discard
			tmp[s->dst] = tmp[s->src];
		}
		if (s->op == 1) { // xor
			if (tmp[s->src]) {
				s->op = -2;
			} else if (tmp[s->dst])
				s->op = 0;
			tmp[s->dst] &= tmp[s->src];
		}
		if (s->op == 2) { // zero
			s->op = -2;
			tmp[s->dst] = 1;
		}
	}
	// dead code elimination
	memset(tmp, 0, rows + cols);
	for (i = 0; i < n * w; i++)
		if (erased[i / w]) tmp[i] = 1;
	for (s--; s >= sched; s--) {
		if (s->op == -2)
			continue;
		if (!tmp[s->dst])
			s->op = -2;
		else {
			if (s->op == 0)
				tmp[s->dst] = 0;
			tmp[s->src] = 1;
		}
	}
	// regenerate
	sched_t *t = sched;
	for (s = sched; s->op != -1; s++)
		if (s->op != -2) {
			t->op = s->op;
			t->src = s->src;
			t->dst = s->dst;
			t++;
		}
	t->op = -1;
	free(tmp);
	return sched;
}

static sched_t* bitmatrix_scheduling(int *bitm, int rows, int cols, int *rbase, int *cbase, sched_t *sched)
{
	sched_t *head = sched;
	int *bptr = bitm, i, j, r;
	int *space = (int*) malloc(sizeof(int) * rows * 4);
	int *ones = space + rows * 0;
	int *prev = space + rows * 1;
	int *visit = space + rows * 2;
	int *order = space + rows * 3;
	memset(ones, -1, sizeof(int) * rows);
	memset(prev, -1, sizeof(int) * rows);
	memset(visit, 0, sizeof(int) * rows);
	for (i = 0; i < rows; i++)
		for (j = 0; j < cols; j++)
			ones[i] += *bptr++;
	int total = 0;
	for (r = 0; r < rows; r++) {
		int ch = -1;
		for (i = 0; i < rows; i++)
			if (!visit[i] && (ch == -1 || ones[i] < ones[ch]))
				ch = i;
		order[r] = ch;
		visit[ch] = 1;
		total += ones[ch] + 1;
		int *p = bitm + ch * cols;
		for (i = 0; i < rows; i++)
			if (!visit[i]) {
				int dif = 0, *q = bitm + i * cols;
				for (j = 0; j < cols; j++)
					dif += p[j] ^ q[j];
				if (dif < ones[i]) {
					ones[i] = dif;
					prev[i] = ch;
				}
			}
	}
	for (r = 0; r < rows; r++) {
		int ch = order[r], op = 0, *p = bitm + ch * cols;
		if (prev[ch] == -1) {
			for (i = 0; i < cols; i++)
				if (p[i]) {
					sched->op = op;
					sched->src = cbase[i];
					sched->dst = rbase[ch];
					op = 1;
					sched++;
				}
		} else {
			int *q = bitm + prev[ch] * cols;
			sched->op = 0;
			sched->src = rbase[prev[ch]];
			sched->dst = rbase[ch];
			sched++;
			for (i = 0; i < cols; i++)
				if (p[i] ^ q[i]) {
					sched->op = 1;
					sched->src = cbase[i];
					sched->dst = rbase[ch];
					sched++;
				}
		}
	}
	sched->op = -1;
	free(space);
	return sched;
}

static int pcm_schedule_bitmatrix(int *bitm, int rows, int cols)
{
	int i, j, r;
	int *ones = (int*) malloc(sizeof(int) * rows);
	int *visit = (int*) malloc(sizeof(int) * rows);
	memset(ones, -1, sizeof(int) * rows);
	memset(visit, 0, sizeof(int) * rows);
	int *p = bitm;
	for (i = 0; i < rows; i++)
		for (j = 0; j < cols; j++)
			ones[i] += *p++;
	int ret = 0;
	for (r = 0; r < rows; r++) {
		int ch = -1;
		for (i = 0; i < rows; i++)
			if (!visit[i] && (ch == -1 || ones[i] < ones[ch]))
				ch = i;
		visit[ch] = 1;
		ret += ones[ch];
		int *p = bitm + ch * cols;
		for (i = 0; i < rows; i++)
			if (!visit[i]) {
				int dif = 0, *q = bitm + i * cols;
				for (j = 0; j < cols; j++)
					dif += p[j] ^ q[j];
				if (dif < ones[i])
					ones[i] = dif;
			}
	}
	free(ones);
	free(visit);
	return ret;
}

int pcm_count_encoding(coding_t *coding)
{
	int rows = coding->rows;
	int cols = coding->cols;
	int *pcm = coding->pcm;
	int i, j, ret = -2 * rows;
	for (i = 0; i < rows * cols; i++)
		ret += pcm[i];
	return ret;
}

sched_t *pcm_encoding_schedule(coding_t *coding)
{
	int rows = coding->rows, *row_info = coding->row_info;
	int cols = coding->cols, *col_info = coding->col_info;
	int *pcm = coding->pcm;
	int *space = (int*) malloc(sizeof(int) * rows * 2);
	int *degree = space, *queue = space + rows;
	memset(degree, 0, sizeof(int) * rows);
	int i, j, front = 0, back = 0;
	for (i = 0; i < rows; i++)
		for (j = 0; j < rows; j++)
			if (j != i && pcm[j * cols + row_info[i]])
				degree[j] += 1;
	for (i = 0; i < rows; i++)
		if (degree[i] == 0)
			queue[back++] = i;
	int total = 1;
	while (front < back) {
		int rowno = queue[front++];
		int *r = pcm + rowno * cols;
		for (i = 0; i < cols; i++)
			if (i != row_info[rowno] && r[i])
				total++;
		for (i = 0; i < rows; i++)
			if (i != rowno && pcm[i * cols + row_info[rowno]])
				if ((--degree[i]) == 0)
					queue[back++] = i;
	}
	assert(front == rows);
	sched_t *sched = (sched_t*) malloc(sizeof(sched_t) * total), *head = sched;
	for (i = 0; i < rows; i++) {
		int rowno = queue[i], op = 0;
		int *r = pcm + rowno * cols;
		for (j = 0; j < cols; j++)
			if (j != row_info[rowno] && r[j]) {
				sched->op = op;
				sched->src = j;
				sched->dst = row_info[rowno];
				sched++;
				op = 1;
			}
	}
	sched->op = -1;
	free(space);
	return head;
}

sched_t *pcm_decoding_schedule(coding_t *coding, int *erased)
{
	int n = coding->n, w = coding->w, m = coding->m;
	int rows = coding->rows, *row_info = coding->row_info;
	int cols = coding->cols, *col_info = coding->col_info;
	int *pcm = coding->pcm;
	int total = w * w * m * m;
	int i, j, f = 0, lost = 0;
	for (i = 0; i < rows * cols; i++)
		total += pcm[i];
	sched_t *sched = (sched_t*) malloc(sizeof(sched_t) * total), *head = sched;
	for (i = 0; i < n; i++)
		if (erased[i]) f++; // number of failed disks
	int *cbase = (int*) malloc(sizeof(int) * rows);
	int *rbase = (int*) malloc(sizeof(int) * rows);
	for (i = 0; i < rows; i++)
		cbase[i] = cols + i;
	for (i = 0; i < n * w; i++)
		if (col_info[i] != -2 && erased[i / w])
			rbase[lost++] = i;
	for (i = n * w; i < cols; i++)
		rbase[lost++] = i;
	int *decm = (int*) malloc(sizeof(int) * rows * rows);
	for (i = 0; i < rows; i++) {
		int *pcmr = pcm + i * cols, op = 0;
		for (j = 0; j < n * w; j++)
			if (pcmr[j] && col_info[j] != -2 && !erased[j / w]) {
				sched->op = op;
				sched->src = j;
				sched->dst = cols + i;
				sched++;
				op = 1;
			}
	}
	int *er = (int*) malloc(sizeof(int) * n); // copy of erased;
	int *cl = (int*) malloc(sizeof(int) * n); // elements in column
	memcpy(er, erased, sizeof(int) * n);
	memset(cl, 0, sizeof(int) * n);
	for (i = 0; i < n * w; i++)
		if (col_info[i] != -2)
			cl[i / w] += 1;
	int has_S = cols - n * w; // number of extra elements
	while (1) {
		int *d = decm;
		for (i = 0; i < rows; i++) {
			int *pcmr = pcm + i * cols;
			for (j = 0; j < n * w + has_S; j++)
				if (j >= n * w || (col_info[j] != -2 && er[j / w]))
					*d++ = pcmr[j];
		}
		pseudo_inverse(decm, rows, lost); // make decm a (lost, rows) matrix
		if (f <= 2 || (!has_S && n < 8)) {
			sched = bitmatrix_scheduling(decm, lost, rows, rbase, cbase, sched);
			break;
		} else {
			if (has_S) {
				sched = bitmatrix_scheduling(decm + (lost - has_S) * rows, has_S, rows, rbase + (lost - has_S), cbase, sched);
				for (i = 0; i < rows; i++)
					for (j = n * w; j < cols; j++)
						if (pcm[i * cols + j]) {
							sched->op = 1;
							sched->src = j;
							sched->dst = cols + i;
							sched++;
						}
				lost -= has_S;
				has_S = 0;
			} else {
				int ch = -1, val = -1, lines = 0, start = -1;
				for (i = 0; i < n; i++)
					if (er[i]) {
						int cur = pcm_schedule_bitmatrix(decm + lines * rows, cl[i], rows);
						if (ch == -1 || cur < val) {
							ch = i;
							val = cur;
							start = lines;
						}
						lines += cl[i];
					}
				sched = bitmatrix_scheduling(decm + start * rows, cl[ch], rows, rbase + start, cbase, sched);
				for (i = start; i < lost - cl[ch]; i++)
					rbase[i] = rbase[i + cl[ch]];
				for (i = 0; i < rows; i++)
					for (j = ch * w; j < (ch + 1) * w; j++)
						if (col_info[j] != -2)
							if (pcm[i * cols + j]) {
								sched->op = 1;
								sched->src = j;
								sched->dst = cols + i;
								sched++;
							}
				er[ch] = 0;
				f -= 1;
				lost -= cl[ch];
			}
		}
	}
	sched->op = -1;
	head = optimize_decode_schedule(coding, head, erased);
	free(er);
	free(cl);
	free(rbase);
	free(cbase);
	free(decm);
	return head;
}

void pcm_copy(coding_t *coding, char *data, int size, char **ptr, int packetsize)
{
	int pos, i, w = coding->w, l = coding->data_len;
	char **dst = (char**) malloc(sizeof(char*) * l);
	for (i = 0; i < l; i++) {
		int pl = coding->data_place[i];
		dst[i] = ptr[pl / w] + (pl % w) * packetsize;
	}
	for (pos = 0; pos < size; pos += l * packetsize) {
		for (i = 0; i < l; i++) {
			memcpy(dst[i], data + i * packetsize, packetsize);
			dst[i] += w * packetsize;
		}
		data += l * packetsize;
	}
	free(dst);
}

void pcm_recover(coding_t *coding, char *data, int size, char **ptr, int packetsize)
{
	int pos, i, w = coding->w, l = coding->data_len;
	char **src = (char**) malloc(sizeof(char*) * l);
	for (i = 0; i < l; i++) {
		int pl = coding->data_place[i];
		src[i] = ptr[pl / w] + (pl % w) * packetsize;
	}
	for (pos = 0; pos < size; pos += l * packetsize) {
		for (i = 0; i < l; i++) {
			memcpy(data + i * packetsize, src[i], packetsize);
			src[i] += w * packetsize;
		}
		data += l * packetsize;
	}
	free(src);
}

void pcm_encode(coding_t *coding, char **ptr, int blocksize, int packetsize, sched_t *sched)
{
	int n = coding->n, w = coding->w, cols = coding->cols;
	int extra = cols - n * w, i, j, pos;
	char **buf = (char**) malloc(sizeof(char*) * cols), *exbuf = NULL;
	for (j = 0; j < n; j++)
		for (i = 0; i < w; i++)
			buf[ID(i, j)] = ptr[j] + i * packetsize;
	if (extra) {
		exbuf = (char*) malloc(sizeof(char) * packetsize * extra);
		for (i = 0; i < extra; i++)
			buf[n * w + i] = exbuf + i * packetsize;
	}
	for (pos = 0; pos < blocksize; pos += w * packetsize) {
		do_scheduled_operations(sched, buf, packetsize);
		for (i = 0; i < n * w; i++)
			buf[i] += w * packetsize;
	}
	free(buf);
	if (extra)
		free(exbuf);
}

void pcm_decode(coding_t *coding, char **ptr, int blocksize, int packetsize, sched_t *sched)
{
	int n = coding->n, w = coding->w;
	int rows = coding->rows, cols = coding->cols;
	char **buf = (char**) malloc(sizeof(char*) * (rows + cols));
	char *exbuf = (char*) malloc((rows + cols - n * w) * packetsize);
	int i, j, pos;
	for (j = 0; j < n; j++)
		for (i = 0; i < w; i++)
			buf[ID(i, j)] = ptr[j] + i * packetsize;
	for (i = 0; i < rows + cols - n * w; i++)
		buf[i + n * w] = exbuf + i * packetsize;
	for (pos = 0; pos < blocksize; pos += w * packetsize) {
		do_scheduled_operations(sched, buf, packetsize);
		for (i = 0; i < n * w; i++)
			buf[i] += w * packetsize;
	}
	free(buf);
	free(exbuf);
}

int pcm_count_decoding_dumb(coding_t *coding, int *erased)
{
	int rows = coding->rows;
	int cols = coding->cols;
	int *row_info = coding->row_info;
	int *col_info = coding->col_info;
	int *pcm = coding->pcm;
	int n = coding->n, w = coding->w;
	int i, j, cc = cols - n * w;
	for (i = 0; i < n * w; i++)
		if (col_info[i] != -2 && erased[i / w])
			cc++; // number of erased elements
	int *decm = (int*) malloc(sizeof(int) * rows * cc), *d = decm;
	int ret = 0;
	for (i = 0; i < rows; i++) {
		int cur = -1, *pcmr = pcm + i * cols;
		for (j = 0; j < cols; j++) {
			if (j >= n * w || (col_info[j] != -2 && erased[j / w]))
				*d++ = pcmr[j];
			if (j < n * w && col_info[j] != -2 && !erased[j / w])
				cur += pcmr[j];
		}
		ret += max(0, cur);
	}
	pcm_rank_bitmatrix(decm, rows, cc);
	invert_bitmatrix(decm, cc);
	ret += pcm_schedule_bitmatrix(decm, cc, cc);
	return ret;
}

int pcm_count_decoding(coding_t *coding, int *erased)
{
	int rows = coding->rows;
	int cols = coding->cols;
	int *row_info = coding->row_info;
	int *col_info = coding->col_info;
	int *pcm = coding->pcm;
	int n = coding->n, w = coding->w;
	int i, j, cc = cols - n * w, f = 0;
	for (i = 0; i < n; i++)
		if (erased[i]) f++;
	for (i = 0; i < n * w; i++)
		if (col_info[i] != -2 && erased[i / w])
			cc++; // number of erased elements
	int *decm = (int*) malloc(sizeof(int) * rows * cc);
	int ret = 0;
	for (i = 0; i < rows; i++) {
		int cur = -1, *pcmr = pcm + i * cols;
		for (j = 0; j < cols; j++)
			if (j < n * w && col_info[j] != -2 && !erased[j / w])
				cur += pcmr[j];
		ret += max(0, cur);
	}
	int *er = (int*) malloc(sizeof(int) * n);
	int *cl = (int*) malloc(sizeof(int) * n);
	memcpy(er, erased, sizeof(int) * n);
	memset(cl, 0, sizeof(int) * n);
	for (i = 0; i < n * w; i++)
		if (col_info[i] != -2)
			cl[i / w] += 1;
	int has_S = cols - n * w;
	while (1) {
		int *d = decm;
		for (i = 0; i < rows; i++) {
			int *pcmr = pcm + i * cols;
			for (j = 0; j < n * w + has_S; j++)
				if (j >= n * w || (col_info[j] != -2 && er[j / w]))
					*d++ = pcmr[j];
		}
		pcm_rank_bitmatrix(decm, rows, cc);
		invert_bitmatrix(decm, cc);
		if (f <= 2) {
			ret += pcm_schedule_bitmatrix(decm, cc, cc);
			break;
		} else {
			if (has_S) {
				ret += pcm_schedule_bitmatrix(decm + (cc - has_S) * cc, has_S, cc);
				for (i = 0; i < rows; i++)
					for (j = n * w; j < cols; j++)
						ret += pcm[i * cols + j];
				cc -= has_S;
				has_S = 0;
			} else {
				int ch = -1, val = -1, lines = 0;
				for (i = 0; i < n; i++)
					if (er[i]) {
						int cur = pcm_schedule_bitmatrix(decm + lines * cc, cl[i], cc);
						if (ch == -1 || cur < val) {
							ch = i;
							val = cur;
						}
						lines += cl[i];
					}
				ret += val;
				for (i = 0; i < rows; i++)
					for (j = ch * w; j < (ch + 1) * w; j++)
						if (col_info[j] != -2)
							ret += pcm[i * cols + j];
				er[ch] = 0;
				f -= 1;
				cc -= cl[ch];
			}
		}
	}
	free(er);
	free(cl);
	free(decm);
	return ret;
}

void pcm_free_coding(coding_t *coding)
{
	free(coding->pcm);
	free(coding->row_info);
	free(coding->col_info);
	free(coding->data_place);
	free(coding);
}

void pcm_sparse_bitmatrix(coding_t *coding)
{
	// scheduling
	int rows = coding->rows;
	int cols = coding->cols;
	int *pcm = coding->pcm;
	int *row_info = coding->row_info;
	int *col_info = coding->col_info;
	int *space = (int*) malloc(sizeof(int) * 5 * rows);
	int *ones = space + rows * 0;
	int *prev = space + rows * 1;
	int *visit = space + rows * 2;
	int *order = space + rows * 3;
	int *degree = space + rows * 4;
	int i, j, r, updated = 1;
	while (updated) {
		memset(ones, -1, sizeof(int) * rows);
		memset(prev, -1, sizeof(int) * rows);
		memset(visit, 0, sizeof(int) * rows);
		memset(degree,0, sizeof(int) * rows);
		for (i = 0; i < rows; i++)
			for (j = 0; j < cols; j++)
				if (j != row_info[i])
					ones[i] += pcm[i * cols + j];
		for (i = 0; i < rows; i++)
			for (j = 0; j < rows; j++)
				if (j != i && pcm[j * cols + row_info[i]])
					degree[j] += 1;
		for (r = 0; r < rows; r++) {
			int ch = -1;
			for (i = 0; i < rows; i++)
				if (!visit[i] && !degree[i] && (ch == -1 || ones[i] < ones[ch]))
					ch = i;
			visit[ch] = 1;
			order[r] = ch;
			for (i = 0; i < rows; i++)
				if (i != ch && pcm[i * cols + row_info[ch]])
					--degree[i];
			int *p = pcm + ch * cols;
			for (i = 0; i < rows; i++)
				if (!visit[i] && !p[row_info[i]]) {
					int *q = pcm + i * cols, diff = -2;
					for (j = 0; j < cols; j++)
						diff += (p[j] ^ q[j]);
					if (diff < ones[i]) {
						ones[i] = diff;
						prev[i] = ch;
					}
				}
		}
		updated = 0;
		for (r = rows - 1; r > -1; r--) {
			int rowno = order[r];
			if (prev[rowno] != -1) {
				updated = 1;
				int *p = pcm + rowno * cols;
				int *q = pcm + prev[rowno] * cols;
				for (i = 0; i < cols; i++)
					p[i] ^= q[i];
			}
		}
	}
	free(space);
}
