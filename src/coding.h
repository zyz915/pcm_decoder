/*
 * coding.h
 *
 *  Created on: Nov 9, 2014
 *      Author: zyz915
 */

#ifndef CODING_H_
#define CODING_H_

#include "pcm_based.h"

enum codings {
	EVENODD,
	RDP,
	X_code,
	H_code,
	Liberation,
	STAR,
	Triple_Star,
	HoVer,
	TIP_code,
	EH_code,
	HDD1
};

coding_t* make_coding(int tech, int n);
const char *coding_name(int tech);
void list_coding_techniques(FILE *f);

#endif /* CODING_H_ */
