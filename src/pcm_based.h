/*
 * pcm_based.h
 *
 *  Created on: Oct 15, 2014
 *      Author: zyz915
 */

#ifndef PCM_BASED_H_
#define PCM_BASED_H_

#define max(x, y) ((x) > (y) ? (x) : (y))
#define min(x, y) ((x) < (y) ? (x) : (y))

typedef struct coding {
	int n; // number of disks
	int m; // fault tolerance
	int w; // word size

	int k; // number of data disks (NOT USED)
	int p; // the prime number     (NOT USED)

	int rows, cols;
	int *pcm; // array[rows][cols], parity check matrix

	int *row_info;
	int *col_info;

	int data_len;
	int *data_place;
} coding_t;

typedef struct schedule {
	int op; // 0 = copy, 1 = xor, 2 = zero, -1 = terminate
	int src, dst;
} sched_t;

int pcm_is_prime(int n);

void pcm_print_bitmatrix(int *bitm, int rows, int cols);

void pcm_free_coding(coding_t *coding);

void pcm_sparse_bitmatrix(coding_t *coding);

sched_t* pcm_encoding_schedule(coding_t *coding);
sched_t* pcm_decoding_schedule(coding_t *coding, int *erased);
void pcm_copy(coding_t *coding, char *data, int size, char **ptr, int packetsize);
void pcm_recover(coding_t *coding, char *data, int size, char **ptr, int packetsize);
void pcm_encode(coding_t *coding, char **ptr, int blocksize, int packetsize, sched_t *sched);
void pcm_decode(coding_t *coding, char **ptr, int blocksize, int packetsize, sched_t *sched);

int pcm_count_encoding(coding_t *coding);
int pcm_count_decoding_dumb(coding_t *coding, int *erased);
int pcm_count_decoding(coding_t *coding, int *erased);

#endif /* PCM_BASED_H_ */
