/*
 * validate.c
 *
 *  Created on: Nov 9, 2014
 *      Author: zyz915
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pcm_based.h"
#include "coding.h"

int usage(char *main)
{
	printf("usage: %s coding_technique n\n\n", main);
	list_coding_techniques(stderr);
	return 0;
}

int xors_in_schedule(sched_t *sched)
{
	int ret = 0;
	for (; sched->op != -1; sched++)
		if (sched->op == 1) ret++;
	return ret;
}

int main(int argc, char **argv)
{
	if (argc < 3)
		return usage(argv[0]);
	int tech, n = atoi(argv[2]), packetsize = 8, m = -1, w = 3;
	if (!strcmp(argv[1], "rdp"))
		tech = RDP;
	else if (!strcmp(argv[1], "evenodd"))
		tech = EVENODD;
	else if (!strcmp(argv[1], "star"))
		tech = STAR;
	else if (!strcmp(argv[1], "triple_star"))
		tech = Triple_Star;
	else if (!strcmp(argv[1], "liberation"))
		tech = Liberation;
	else if (!strcmp(argv[1], "hover"))
		tech = HoVer;
	else if (!strcmp(argv[1], "tip"))
		tech = TIP_code;
	else if (!strcmp(argv[1], "xhcode"))
		tech = EH_code;
	else if (!strcmp(argv[1], "hdd1"))
		tech = HDD1;
	else {
		fprintf(stderr, "Unrecognized coding technique: %s\n", argv[1]);
		return 0;
	}
	coding_t *coding = NULL;
	coding = make_coding(tech, n);

	int stripes = 2, i, j, k;
	int datasize = stripes * coding->data_len * packetsize;
	int blocksize = stripes * coding->w * packetsize;
	char *data = (char*) malloc(sizeof(char) * datasize);
	char **dev = (char**) malloc(sizeof(char*) * n);
	char **snapshot = (char**) malloc(sizeof(char*) * n);
	for (i = 0; i < n; i++) {
		dev[i] = (char*) malloc(sizeof(char) * blocksize);
		snapshot[i] = (char*) malloc(sizeof(char) * blocksize);
	}
	for (i = 0; i < datasize; i++)
		data[i] = (rand() & 1);
	sched_t *enc = pcm_encoding_schedule(coding);
	pcm_copy(coding, data, datasize, dev, packetsize);
	pcm_encode(coding, dev, blocksize, packetsize, enc);
	for (i = 0; i < n; i++)
		memcpy(snapshot[i], dev[i], sizeof(char) * blocksize);
	int *erased = (int*) malloc(sizeof(int) * n), cas = 0;
	long long total_xors = 0, cc = 0;
	for (i = 1; i < (1 << n); i++) {
		memset(erased, 0, sizeof(int) * n);
		int f = 0;
		for (j = 0; j < n; j++)
			if (1 & (i >> j))
				f += erased[j] = 1;
		if (f > coding->m) continue;
		cas++;
		for (j = 0; j < n; j++)
			if (erased[j])
				memset(dev[j], 48, sizeof(char) * blocksize);
		sched_t *sched = pcm_decoding_schedule(coding, erased);
		pcm_decode(coding, dev, blocksize, packetsize, sched);
		if (f == coding->m) {
			total_xors += xors_in_schedule(sched);
			cc += 1;
		}
		free(sched);
		int valid = 1;
		for (j = 0; j < n; j++)
			if (strncmp(dev[j], snapshot[j], blocksize) != 0)
				valid = 0;
		if (!valid) {
			printf("testcase #%d failed.\n", cas);
			printf("E:");
			for (j = 0; j < n; j++)
				printf(" %d", erased[j]);
			printf("\n");
			for (j = 0; j < n; j++) {
				printf("dev #%d: ", j);
				for (k = 0; k < blocksize; k++) {
					if (k % packetsize == 0)
						printf(" ");
					printf("%c", dev[j][k] + 48);
				}
				printf("\n");
			}
			printf("\n");
			return 0;
		}
	}
	printf("%2d %8.4f %8.4f  %s %d %d\n", n, xors_in_schedule(enc) * 1.0 / coding->data_len,
			total_xors * 1.0 / cc / coding->data_len, coding_name(tech), coding->m, coding->w);
	return 0;
}
