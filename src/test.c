/*
 * test.c
 *
 *  Created on: Oct 15, 2014
 *      Author: zyz915
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pcm_based.h"
#include "coding.h"

int main(int argc, char **argv)
{
	if (argc != 3) {
		fprintf(stderr, "usage: %s coding_technique n\n\n", argv[0]);
		list_coding_techniques(stderr);
		return 0;
	}
	int n = atoi(argv[2]), tech;
	if (!strcmp(argv[1], "rdp"))
		tech = RDP;
	else if (!strcmp(argv[1], "evenodd"))
		tech = EVENODD;
	else if (!strcmp(argv[1], "star"))
		tech = STAR;
	else if (!strcmp(argv[1], "triple_star"))
		tech = Triple_Star;
	else if (!strcmp(argv[1], "liberation"))
		tech = Liberation;
	else if (!strcmp(argv[1], "hover"))
		tech = HoVer;
	else if (!strcmp(argv[1], "tip"))
		tech = TIP_code;
	else if (!strcmp(argv[1], "xhcode"))
		tech = EH_code;
	else if (!strcmp(argv[1], "hdd1"))
		tech = HDD1;
	else {
		fprintf(stderr, "invalid coding technique: %s\n", argv[1]);
		return 0;
	}

	coding_t *coding = make_coding(tech, n);
	double enc = pcm_count_encoding(coding);
	double dec = 0, dec_IR = 0;
	int len = coding->data_len, cnt = 0;
	int i, j, k, *erased = (int*) malloc(sizeof(int) * n);
	if (coding->m == 2)
		for (i = 0; i < n; i++)
			for (j = i + 1; j < n; j++) {
				memset(erased, 0, sizeof(int) * n);
				erased[i] = erased[j] = 1;
				dec += pcm_count_decoding_dumb(coding, erased);
				dec_IR += pcm_count_decoding(coding, erased);
				cnt += 1;
			}
	if (coding->m == 3)
		for (i = 0; i < n; i++)
			for (j = i + 1; j < n; j++)
				for (k = j + 1; k < n; k++) {
					memset(erased, 0, sizeof(int) * n);
					erased[i] = erased[j] = erased[k] = 1;
					dec += pcm_count_decoding_dumb(coding, erased);
					dec_IR += pcm_count_decoding(coding, erased);
					cnt += 1;
				}
	dec /= cnt;
	dec_IR /= cnt;
	printf("%4d %10.6f %10.6f %10.6f %s\n", n, enc / len, dec / len, dec_IR / len, coding_name(tech));

	pcm_free_coding(coding);
	free(erased);
	return 0;
}
