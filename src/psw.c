/*
 * psw.c
 *
 *  Created on: Oct 17, 2014
 *      Author: zyz915
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pcm_based.h"
#include "coding.h"

int calc(coding_t *coding, int start, int end)
{
	int ret = 0, i, j;
	int rows = coding->rows, cols = coding->cols;
	int *pcm = coding->pcm;
	int *r = (int*) malloc(sizeof(int) * rows);
	memset(r, 0, sizeof(int) * rows);
	for (i = start; i < end; i++)
		for (j = 0; j < rows; j++)
			if (pcm[j * cols + coding->data_place[i]])
				r[j] = 1;
	while (1) {
		int cur = 0;
		for (i = 0; i < rows; i++)
			if (r[i]) cur++;
		if (cur == ret) break;
		ret = cur;
		for (i = 0; i < rows; i++)
			if (r[i])
				for (j = 0; j < rows; j++)
					if (pcm[j * cols + coding->row_info[i]])
						r[j] = 1;
	}
	free(r);
	return ret;
}

int main(int argc, char **argv)
{
	if (argc != 4) {
		fprintf(stderr, "usage: %s coding_technique n w\n", argv[0]);
		return 0;
	}
	int n = atoi(argv[2]), w = atoi(argv[3]), tech;
	if (!strcmp(argv[1], "rdp"))
		tech = RDP;
	else if (!strcmp(argv[1], "evenodd"))
		tech = EVENODD;
	else if (!strcmp(argv[1], "star"))
		tech = STAR;
	else if (!strcmp(argv[1], "triple_star"))
		tech = Triple_Star;
	else if (!strcmp(argv[1], "liberation"))
		tech = Liberation;
	else if (!strcmp(argv[1], "hover"))
		tech = HoVer;
	else if (!strcmp(argv[1], "tip"))
		tech = TIP_code;
	else if (!strcmp(argv[1], "xhcode"))
		tech = EH_code;
	else if (!strcmp(argv[1], "hdd1"))
		tech = HDD1;
	else {
		fprintf(stderr, "Unrecognized coding technique: %s\n", argv[1]);
		return 0;
	}

	coding_t *coding = make_coding(tech, n);
	int i, psw = 0;
	for (i = 0; i < coding->data_len; i++) {
		int s = 0, cur = 0;
		while (s < i + w) {
			cur += calc(coding, max(0, i - s), min(coding->data_len, i + w - s));
			s += coding->data_len;
		}
		psw += cur;
	}
	printf("%4d %4d %10.6f %s\n", n, w, psw * 1.0 / coding->data_len,
			coding_name(tech));

	pcm_free_coding(coding);
	return 0;
}
